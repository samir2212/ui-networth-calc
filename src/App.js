import React, { useState, useEffect } from 'react';
import { createBrowserHistory } from 'history';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';

import SideNav from './sidebar';
import Calculator from './containers/calculator';

const { Header, Sider, Content } = Layout;

export const history = createBrowserHistory();

const App = () => {
  const [collapse, setCollapse] = useState(false);
  useEffect(() => {
    window.innerWidth <= 760 ? setCollapse(true) : setCollapse(false);
  }, []);
  const handleToggle = (event) => {
    event.preventDefault();
    collapse ? setCollapse(false) : setCollapse(true);
  };
  return (
    <Layout>
        <Sider trigger={null} collapsible collapsed={collapse}>
          <SideNav />
        </Sider>
        <Layout>
        <Header className="siteLayoutBackground" style={{ padding: 0, background: '#001529' }}>
          {React.createElement(collapse ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: handleToggle,
            style: { color: '#fff' },
          })}
        </Header>
        <Content style={{
          padding: 24, minHeight: 'calc(100vh - 114px)', background: '#f0f2f5',
        }}
        >
          <Calculator />
        </Content>
        </Layout>
      </Layout>
  );
}

export default connect()(App);
