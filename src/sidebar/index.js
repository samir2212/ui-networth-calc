import React, { Component } from 'react';
import { Menu } from 'antd';
import styled from 'styled-components';
import {
  DashboardOutlined,
} from '@ant-design/icons';

class SideNav extends Component {
  render() {
    return (
      <div>
        <Logo>
          Net Worth Calculator
        </Logo>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
          <Menu.Item key="1">
            <DashboardOutlined />
            <span>Calculator</span>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}

export default SideNav;

const Logo = styled.div`
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
  color: white;
  text-align: center;
  padding-top: 5px;
`;
