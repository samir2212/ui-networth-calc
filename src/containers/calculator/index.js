import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Row, Col, Divider, Select } from 'antd';
import Field from '../../components/Field';
import LiabilityField from '../../components/LiabilityField';
import { calculateNetWorth } from './ducks';

const MainArea = styled.div`
  background: white;
  padding: 1.5em;
  width: 70%;
  display: inline-block;
`;

const currencyOptions = [
  {label: 'CAD', value: 'CAD', symbol: 'CA$'},
  {label: 'USD', value: 'USD', symbol: '$'},
  {label: 'EUR', value: 'EUR', symbol: '€'},
  {label: 'INR', value: 'INR', symbol: '₹'},
  {label: 'GBP', value: 'GBP', symbol: '£'},
  {label: 'HKD', value: 'HKD', symbol: 'HK$'},
  {label: 'AUD', value: 'AUD', symbol: 'A$'},
  {label: 'JPY', value: 'JPY', symbol: '¥'},
  {label: 'BRL', value: 'BRL', symbol: 'R$'},
  {label: 'MYR', value: 'MYR', symbol: 'RM'},
  
];

class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currency:'CAD',
      previousCurrency: 'CAD',
      currencySymbol: 'CA$',
      assets: {},
      liabilities: {},
    };
  }
  calculate = (isConversionRequired) => {
    const {
      currency,
      assets,
      liabilities,
      previousCurrency,
    } = this.state;

    const payload = {
      assets,
      liabilities,
      currency,
      fromCurrency: previousCurrency,
    };
    this.props.calculateNetWorth(payload, isConversionRequired, this);
  }
  formatNumber = (value) => {
    const amount = value ? value.replace(/\D+/g, '') : 0;
    return isNaN(Number(amount)) ? 0 : Number(amount);
  }
  formatMoney = number => {
    return number.toLocaleString('en-US', { style: 'currency', currency: this.state.currency });
  }
  render() {
    return (
      <div>
        <MainArea>
          <h2>Tracking your Networth</h2>
          <div id="assets">
            <Row>
              <Col span={18}></Col>
              <Col span={6}>
                Select currency:&nbsp;
              <Select
                defaultValue="CAD"
                style={{ width: 120 }}
                onChange={value => {
                  const previousCurrency = (' ' + this.state.currency).slice(1);
                  this.setState({
                    previousCurrency,
                    currency: value,
                    currencySymbol: currencyOptions.find( option => option.value === value).symbol,
                    }, () => this.calculate(true))
                  }
                }
                options={currencyOptions}
              >
              </Select>
              </Col>
            </Row>
            <Row>
                <Col span={18} style={{fontWeight:"bold", fontSize:"16px", color:"green" }}>Net Worth</Col>
                <Col span={6} style={{fontWeight:"bold", fontSize:"14px", color:"green" }}>{this.props.totalAssets ? this.formatMoney(this.props.netWorth) : '-'}</Col>
            </Row>
            <Row>
                <Col span={18} style={{fontWeight:"bold", fontSize:"16px", color:"green" }}>Assets</Col>
            </Row>
            <Field
              label="Cash and Investments"
              currency={this.state.currencySymbol}
              onBlur={e =>
                this.setState({
                  assets: {
                    ...this.state.assets, cashInvestments: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
              value={this.props.assets.cashInvestments}
            />
            <Field label="Chequing"
              currency={this.state.currencySymbol}
              onBlur={e =>
                this.setState({
                  assets: {
                    ...this.state.assets, chequing: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.assets.chequing}
            />
            <Field label="Savings for Taxes"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, savingsForTaxes: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.savingsForTaxes}
            />
            <Field label="Rainy Day Fund"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, rainyDay: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.rainyDay}
            />
            <Field label="Savings for Fun"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, savingsForFun: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.savingsForFun}
            />
            <Field label="Savings for Travel"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, savingsForTravel: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.savingsForTravel}
            />
            <Field label="Savings for Personal Development"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, savingsForPersonalDevelopment: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.savingsForPersonalDevelopment}
            />
            <Field label="Investment 1"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  investment1: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.investment1}
            />
            <Field label="Investment 2"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  investment2: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.investment2}
            />
            <Field label="Investment 3"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  investment3: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.investment3}
            />
            <Field label="Investment 4"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  investment4: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.investment4}
            />
            <Field label="Investment 5"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  investment5: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.investment5}
            />
            <Row>
                <Col span={18} style={{fontWeight:"bold", fontSize:"16px"}}>Long Term Assets</Col>
            </Row>
            <Field label="Primary Home"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets, 
                  primaryHome: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.primaryHome}
            />
            <Field label="Secondary Home"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets,
                  secondaryHome: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.secondaryHome}
            />
            <Field label="Other"
              currency={this.state.currencySymbol}
              onBlur={e => this.setState({
                assets: {
                  ...this.state.assets,
                  otherAssets: this.formatNumber(e.target.value),
                }
              }, () => this.calculate())}
              value={this.props.assets.otherAssets}
            />
            <Row>
                <Col span={18} style={{fontWeight:"bold", fontSize:"16px", color:"green" }}>Total Assets</Col>
                <Col span={6} style={{fontWeight:"bold", fontSize:"14px", color:"green" }}>{this.props.totalAssets ? this.formatMoney(this.props.totalAssets) : '-'}</Col>
            </Row>
          </div>
          <div id="liabilities">
            <div id="short-term-liabilities">
              <br/>
              <Row>
                <Col span={24} style={{fontWeight:"bold", fontSize:"16px", color:"green" }}>Liabilities</Col>
              </Row>
              <Row>
                <Col span={12} style={{fontWeight:"bold", fontSize:"16px" }}>Short Term Liabilities</Col>
                <Col span={6} style={{fontWeight:"bold", fontSize:"16px" }}>Monthly Payments</Col>
                <Col span={6}></Col>
              </Row>
              <LiabilityField label="Credit Card 1"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    creditCard1: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.creditCard1}
              />
              <LiabilityField label="Credit Card 2"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    creditCard2: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.creditCard2}
              />
              <Field label="(Others...)"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    otherShortTermLiabilities: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.otherShortTermLiabilities}
              />
            </div>
            <div id="long-term-debts">
              <br/>
              <Row>
                <Col span={24} style={{fontWeight:"bold", fontSize:"16px" }}>Long Term Debt</Col>
              </Row>
              <LiabilityField label="Mortgage 1"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    mortgage1: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.mortgage1}
              />
              <LiabilityField label="Mortgage 2"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    mortgage2: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.mortgage2}
              />
              <LiabilityField label="Line of Credit"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    lineOfCredit: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.lineOfCredit}
              />
              <LiabilityField label="Investment Loan"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    investmentLoan: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.investmentLoan}
              />
              <Field label="Student Loan"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    studentLoan: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.studentLoan}
              />
              <Field label="Car Loan"
                currency={this.state.currencySymbol}
                onBlur={e => this.setState({
                  liabilities: {
                    ...this.state.liabilities,
                    carLoan: this.formatNumber(e.target.value),
                  }
                }, () => this.calculate())}
                value={this.props.liabilities.carLoan}
              />
              <Row>
                  <Col span={18} style={{fontWeight:"bold", fontSize:"16px", color:"green" }}>Total Liabilities</Col>
                  <Col span={6} style={{fontWeight:"bold", fontSize:"14px", color:"green" }}>{this.props.totalAssets ? this.formatMoney(this.props.totalLiabilities) : '-'}</Col>
              </Row>
            </div>
          </div>
        </MainArea>
      </div>
    );
  }
}

Calculator.propTypes = {
  calculateNetWorth: PropTypes.func,
  totalAssets: PropTypes.number,
  totalLiabilities: PropTypes.number,
  netWorth: PropTypes.number,
};
Calculator.defaultProps = {
  calculateNetWorth
};
const mapStateToProps = state => ({
  totalAssets: state.netWorthData.totalAssets,
  totalLiabilities: state.netWorthData.totalLiabilities,
  netWorth: state.netWorthData.netWorth,
  assets: state.netWorthData.assets,
  liabilities: state.netWorthData.liabilities,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    calculateNetWorth,
  },
  dispatch,
);
export default connect(mapStateToProps, mapDispatchToProps)(Calculator);
