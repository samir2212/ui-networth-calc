import Api from '../../utils/api';

export const GET_CALCULATE_RESPONSE = 'calculate/GET_CALCULATE_RESPONSE';
export const GET_CALCULATE_ERROR = 'calculate/GET_CALCULATE_ERROR';

const INITIAL_STATE = {
  totalAssets: undefined,
  totalLiabilities: undefined,
  netWorth: undefined,
  assets: {},
  liabilities: {},
};

export default function reducer(state = INITIAL_STATE, { type, payload }) {
  if (!type) return state;

  switch (type) {
    case GET_CALCULATE_RESPONSE:
      return { ...state, ...payload, error: undefined};
    case GET_CALCULATE_ERROR:
      return { ...state, ...payload, error: 'Oops, something went wrong.'};
    default:
      return state;
  }
}

export function calculateNetWorth(data, isConversionRequired, state) {
  return async dispatch => {
    try {
      const uri = isConversionRequired ? '/calculate?isConversionRequired=true' : '/calculate';
      const response = await Api.post(uri, {...data});
      dispatch({
        type: GET_CALCULATE_RESPONSE,
        payload: response.data,
      });
      state.setState({...response.data});
    } catch (error) {
      dispatch({
        type: GET_CALCULATE_ERROR,
        payload: [],
      });
    }
  }
}
