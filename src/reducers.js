import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import calculateReducer from './containers/calculator/ducks';

const appReducer = combineReducers({
  router: routerReducer,
  netWorthData: calculateReducer,
});

export default appReducer;
