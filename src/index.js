import { hot } from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';
import * as serviceWorker from './serviceWorker';

import reducers from './reducers';
import App, { history } from './App';

import './style/index.scss';
// import './styles/animation.scss';
import './style/antd.custom.css';

const middleware = routerMiddleware(history);

const logger = createLogger();

const store = createStore(
  reducers,
  window.devToolsExtension ? window.devToolsExtension() : f => f,
  process.env.NODE_ENV === 'production'
    ? applyMiddleware(middleware, thunk)
    : applyMiddleware(middleware, thunk, logger),
);

const rootElement = document.getElementById('root');

const HotApp = hot(module)(App);

ReactDOM.render(
  <Provider store={store}>
    <HotApp />
  </Provider>,
  rootElement,
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
