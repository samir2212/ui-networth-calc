import axios from 'axios';

let API_SERVER = 'http://localhost:7890';
const Api = axios.create({
  baseURL: `${API_SERVER}/`,
});

Api.interceptors.request.use(config => config, (error) => {
  Promise.reject(error);
});

export default Api;
