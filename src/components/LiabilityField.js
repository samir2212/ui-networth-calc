import React from 'react';
import {Row, Col, InputNumber} from 'antd';
import PropTypes from 'prop-types';

const LiabilityField = ({ id, label, onBlur, defaultValue, min, max, onMonthlyValueBlur, currency }) => {
  return (
    <Row id={id}>
      <Col span={12}>{label}</Col>
      <Col span={6}>
        <InputNumber
            onBlur={onMonthlyValueBlur}
            size="small"
            style={{ width:"80%" }}
            formatter={value => `${currency} ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\D+/g, '')}
          />
      </Col>
      <Col span={6}>
        <InputNumber 
          min={min}
          max={max}
          defaultValue={defaultValue}
          onBlur={onBlur}
          size="small"
          style={{ width:"80%" }}
          formatter={value => `${currency} ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
          parser={value => value.replace(/\D+/g, '')}
        />
      </Col>
    </Row>
  );
};

LiabilityField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  defaultValue: PropTypes.number,
  onChange: PropTypes.func,
  onMonthlyValueBlur: PropTypes.func,
  monthlyValue: PropTypes.number,
  currency: PropTypes.string,
};

LiabilityField.defaultProps = {
};

export default LiabilityField;