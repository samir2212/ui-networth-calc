import React from 'react';
import {Row, Col, InputNumber} from 'antd';
import PropTypes from 'prop-types';

const Field = ({ id, label, value, onBlur, defaultValue, min, max, style, currency }) => {
  return (
    <Row id={id}>
      <Col span={18} style={style}>{label}</Col>
      <Col span={6}>
        <InputNumber 
          min={min}
          max={max}
          defaultValue={defaultValue}
          onBlur={onBlur}
          size="small"
          style={{ width:"80%" }}
          formatter={value => `${currency} ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
          parser={value => value.replace(/\D+/g, '')}
          value={value}
        />
      </Col>
    </Row>
  );
};

Field.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  defaultValue: PropTypes.number,
  onBlur: PropTypes.func,
  style: PropTypes.string,
  currency: PropTypes.string,
};

Field.defaultProps = {
};

export default Field;